import ReactDOM from 'react-dom'
import React from 'react'
import NavBar from '../src/containers/NavBar/NavBar'
import NavLink from '../src/containers/NavBar/NavLink/NavLink'
import Logo from '../src/containers/NavBar/Logo/Logo'

test('the NavBar component should render', () => {
  expect(ReactDOM.render(<NavBar />, document.createElement('div')))
})

test('the NavLink component should render', () => {
  expect(ReactDOM.render(<NavLink />, document.createElement('div')))
})

test('the NavLink component should render custom content text and link url', () => {
  const div = document.createElement('div')
  const app = ReactDOM.render(<NavLink text="test" link="url" />, div)
  expect(div.innerHTML).toEqual(expect.stringContaining('test'))
  expect(div.innerHTML).toEqual(expect.stringContaining('url'))
})

test('the Logo component should render', () => {
  expect(ReactDOM.render(<Logo />, document.createElement('div')))
})
