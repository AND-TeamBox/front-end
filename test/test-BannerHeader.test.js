import ReactDOM from 'react-dom'
import React from 'react'
import BannerHeader from '../src/containers/BannerHeader/BannerHeader'
import ImgBanner from '../src/containers/BannerHeader/ImgBanner/ImgBanner'
import TextIntro from '../src/containers/BannerHeader/TextIntro/TextIntro'

test('the BannerHeader component should render', () => {
  expect(ReactDOM.render(<BannerHeader />, document.createElement('div')))
})

test('the ImgBanner component should render', () => {
  expect(ReactDOM.render(<ImgBanner />, document.createElement('div')))
})

test('the TextIntro component should render', () => {
  expect(ReactDOM.render(<TextIntro />, document.createElement('div')))
})
