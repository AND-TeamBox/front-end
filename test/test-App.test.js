import ReactDOM from 'react-dom'
import React from 'react'
import App from '../src/containers/App/App'

test('the App component should render', () => {
  expect(ReactDOM.render(<App />, document.createElement('div')))
})
