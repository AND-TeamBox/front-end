import ReactDOM from 'react-dom'
import React from 'react'
import Footer from '../src/containers/Footer/Footer'

test('the Footer component should render', () => {
    expect(ReactDOM.render(<Footer />, document.createElement('div')))
})