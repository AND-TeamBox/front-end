import ReactDOM from 'react-dom'
import React from 'react'
import SearchBar from '../src/containers/SearchBar/SearchBar'
import SearchButton from '../src/containers/SearchBar/SearchButton/SearchButton'
import SearchInput from '../src/containers/SearchBar/SearchInput/SearchInput'

test('the SearchBar component should render', () => {
  expect(ReactDOM.render(<SearchBar />, document.createElement('div')))
})

test('the SearchButton component should render', () => {
  expect(ReactDOM.render(<SearchButton />, document.createElement('div')))
})

test('the SearchInput component should render', () => {
  expect(ReactDOM.render(<SearchInput />, document.createElement('div')))
})
