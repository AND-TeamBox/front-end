import ReactDOM from 'react-dom'
import React from 'react'
import Listing from '../src/containers/Listing/Listing'
import ProfileDetails from '../src/containers/Listing/ProfileDetails/ProfileDetails'
import SkillsList from '../src/containers/Listing/SkillsList/SkillsList'
import Skill from '../src/containers/Listing/SkillsList/Skill/Skill'

test('the ProfileDetails component should render', () => {
  expect(ReactDOM.render(<ProfileDetails />, document.createElement('div')))
})

test('the ProfileDetails component should render the profile details', () => {
  const div = document.createElement('div')
  const app = ReactDOM.render(<ProfileDetails name="John Doe" club="Turing" />, div)
  expect(div.innerHTML).toEqual(expect.stringContaining('John Doe'))
  expect(div.innerHTML).toEqual(expect.stringContaining('Turing'))
})

test('the SkillsList component should render', () => {
  expect(ReactDOM.render(<SkillsList skills={[]} />, document.createElement('div')))
})

test('the Skill component should render', () => {
  expect(ReactDOM.render(<Skill />, document.createElement('div')))
})

test('the Skill component should render the skill level', () => {
  const div = document.createElement('div')
  const app = ReactDOM.render(<Skill skill="javascript" level="senior" mainSkill={true}/>, div)
  expect(div.innerHTML).toEqual(expect.stringContaining('javascript'))
  expect(div.innerHTML).toEqual(expect.stringContaining('senior'))
})

test('the Listing component should render', () => {
  expect(ReactDOM.render(<Listing skills={[]} />, document.createElement('div')))
})
