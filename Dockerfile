FROM node:9.5-alpine

WORKDIR /app

COPY package.json .

COPY Procfile ./
COPY src ./
COPY webpack ./

RUN npm install
CMD npm start
