const webpack = require('webpack')
const path = require('path')

const parentDir = path.join(__dirname, '../src/')

module.exports = {
  node: {
    child_process: 'empty',
    fs: 'empty'
  },
  entry: [
    path.join(parentDir, 'index.js')
  ],
  module: {
    loaders: [{
      test: /\.(js|jsx)$/,
      exclude: /node_modules/,
      loader: 'babel-loader'
    },
    {
      test: /\.css$/,
      exclude: /node_modules/,
      loaders: ['style-loader', {
        loader: 'css-loader',
        options: {
          modules: true
        }
      }]
    },
    {
      test: /\.(woff|woff2|eot|ttf|otf)$/,
      loader: 'file-loader'
    },
    {
      test: /\.(jpe?g|png|gif|svg)$/i,
      use: [
        'url-loader?limit=10000',
        'img-loader'
      ]
    }]
  },
  output: {
    path: parentDir + '/dist',
    filename: 'bundle.js'
  },
  devServer: {
    contentBase: parentDir,
    historyApiFallback: true
  }
}
