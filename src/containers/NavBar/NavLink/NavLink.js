import React, {Component} from 'react'
import styles from './NavLink.css'

/*
  Defines NavLink as a new Component
  @prop {string} link - The link to the separate page in navigation bar
  @prop {string} text - Text describing the name of the pages available in the navigation bar
*/

export default class NavLink extends Component {
  /*
    Function: NavLink.render
    @return {Component} NavLink
  */
  render () {
    return <a className={styles.navLink} href={this.props.link}>{this.props.text}</a>
  }
}
