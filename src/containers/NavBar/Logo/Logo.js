import React, {Component} from 'react'
import styles from './Logo.css'

/*
  Defines Logo as a new Component
*/

export default class Logo extends Component {
  /*
    Function: Logo.render
    @return {Component} Logo
  */
  render () {
    return <a className={styles.logo} href='https://and.digital/' title='And-Digital-logo' />
  }
}
