import React, {Component} from 'react'
import styles from './NavBar.css'
import Logo from './Logo/Logo'
import NavLink from './NavLink/NavLink'

/*
  Defines navBar as a new Component
*/

export default class NavBar extends Component {
  /*
    Function: NavBar.render
    @return {Component} NavBar
  */
  render () {
    return <div className={styles.navBar}>
      <Logo />
      <div className={styles.menuToggle}>
        <input type="checkbox" />
        <span></span>
        <span></span>
        <span></span>
        <ul className={styles.menu}>
          <NavLink text="What We Do" link="#" />
          <NavLink text="Research" link="#" />
          <NavLink text="About" link="#" />
          <NavLink text="Contact" link="#" />
          <NavLink text="Join Us" link="#" />
        </ul>
      </div>
    </div>
  }
}
