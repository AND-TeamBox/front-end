import React, {Component} from 'react'
import styles from './Count.css'

/*
  Defines Count as a new Component
  @prop {number} level - Count of results
*/
export default class Count extends Component {
  /*
    Function: Count.render
    @return {Component} Count
  */
  render () {
    return <div className={styles.count}>
      Results: {this.props.count}
    </div>
  }
}
