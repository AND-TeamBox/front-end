import React, {Component} from 'react'
import styles from './Skill.css'

/*
  Defines Skill as a new Component
  @prop {string} skill - The skill that a person has
  @prop {boolean} mainSkill - Whether this is the skill that was searched for
  @prop {string} level - The level that this person is in the given skill
*/
export default class Skill extends Component {
  /*
    Function: Skill.render
    @return {Component} Skill
  */
  render () {
    console.log(this.props.skill, this.props.level, this.props.mainSkill, styles[this.props.skill])
    return <div className={`${styles[this.props.skill]} ${this.props.mainSkill && styles.mainSkill}`}>
      <span className={styles.skill}>{this.props.skill}:</span> <span className={styles.level}>{this.props.level}</span>
    </div>
  }
}
