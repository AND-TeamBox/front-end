import React, {Component} from 'react'
import styles from './SkillsList.css'
import Skill from './Skill/Skill'

/*
  Defines SkillsList as a new Component
  @prop {object} skills - An object containing all skills for a particular person
*/
export default class SkillsList extends Component {
  /*
    Function: SkillsList.getSkills
    @param {array} skills - An object containing all skills for a particular person
    @return {array} - Array of Skill components, one for each skill in the skills object
  */
  getSkills = (skills) => {
    return skills.map((item, i) => {
      return <Skill skill={item.skill} key={i} level={item.level} mainSkill={i === 0}/>
    })
  }
  /*
    Function: SkillsList.render
    @return {Component} SkillsList
  */
  render () {
    return <div className={styles.SkillsList}>
      {this.getSkills(this.props.skills)}
    </div>
  }
}
