import React, {Component} from 'react'
import styles from './Listing.css'
import ProfileDetails from './ProfileDetails/ProfileDetails'
import SkillsList from './SkillsList/SkillsList'

/*
  Defines Listing as a new Component
  @prop {object} skills - An object containing all skills for a particular person
  @prop {string} name - The name of a particular person
  @prop {string} club - The club that a particular person belongs to
*/
export default class Listing extends Component {
  /*
    Function: Listing.render
    @return {Component} Listing
  */
  render () {
    return <div className={styles.listing}>
      <ProfileDetails name={this.props.name} club={this.props.club}/>
      <SkillsList skills={this.props.skills}/>
    </div>
  }
}
