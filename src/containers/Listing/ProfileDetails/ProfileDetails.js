import React, {Component} from 'react'
import styles from './ProfileDetails.css'

/*
  Defines ProfileDetails as a new Component
  @prop {string} name - The name of a particular person
  @prop {string} club - The club that a particular person belongs to
*/
export default class ProfileDetails extends Component {
  /*
    Function: ProfileDetails.render
    @return {Component} ProfileDetails
  */
  render () {
    return <div className={styles.profileDetails}>
      <img className={styles.profileImage} src={this.props.src || '../../../images/user-placeholder.png'}/>
      <div className={styles.detailsContainer}>
        <h1 className={styles.profileName}>
          {this.props.name}
        </h1>
        <h2 className={styles.profileClub}>
          {this.props.club}
        </h2>
      </div>
    </div>
  }
}
