import React, {Component} from 'react'
import styles from './ImgBanner.css'

/*
  Defines ImgBanner as a new Component
*/
export default class ImgBanner extends Component {
   /*
    Function: ImgBanner.render
    @return {Component} ImgBanner
  */
  render () {
    return <div className={styles.imgBanner}>
      <h2 className={styles.teamboxHeader}>TeamBox</h2>
      <h1 className={styles.tagLine}>
        People <span className={styles.and}>and</span> skills
      </h1>
    </div>
  }
}
