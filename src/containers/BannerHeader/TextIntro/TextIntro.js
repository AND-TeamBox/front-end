import React, {Component} from 'react'
import styles from './TextIntro.css'

/*
  Defines TextIntro as a new Component
*/
export default class TextIntro extends Component {
   /*
    Function: TextIntro.render
    @return {Component} TextIntro
  */
  render () {
    return <div className={styles.textIntro}>
      <div className={styles.textContent}>
        Every ANDi has amazing skills... <span className={styles.introTagline}>Here you can find the match for your needs!</span>
      </div>
    </div>
  }
}
