import React, {Component} from 'react'
import ImgBanner from './ImgBanner/ImgBanner'
import TextIntro from './TextIntro/TextIntro'

/*
  Defines BannerHeader as a new Component
*/
export default class BannerHeader extends Component {
   /*
    Function: BannerHeader.render
    @return {Component} BannerHeader
  */
  render () {
    return <div>
      <ImgBanner />
      <TextIntro />
    </div>
  }
}
