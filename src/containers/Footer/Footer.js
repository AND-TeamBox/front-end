import React, {Component} from 'react'
import styles from './Footer.css'

/*
  Defines Footer as a new Component
*/
export default class Footer extends Component {
  /*
    Function: Footer.render
    @return {Component} Footer
  */
  render () {
    return <div className={styles.footer}>
      <div>Club Turing</div>
      <div>
        25 Luke Street,
      </div>
      <div>
        London, EC2A 4DS
      </div>
      <div>
        <a target='_blank' href='http://maps.google.co.uk/?q=London+EC2A+4DS'>
          View location
        </a>
      </div>
    </div>
  }
}
