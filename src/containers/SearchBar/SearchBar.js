import React, {Component} from 'react'
import styles from './SearchBar.css'
import SearchInput from './SearchInput/SearchInput'
import SearchButton from './SearchButton/SearchButton'

/*
  Defines SearchBar as a new Component
  @prop {function} search - Searches for typed in skill
*/

export default class SearchBar extends Component {
  /*
    Function: SearchBar.updateValue
    @param {string} value - Typed in skill in search box
  */
  updateValue = (value) => {
    this.value = value
  }
  /*
    Function: SearchBar.searchSkill
    @param {string} value - Typed in skill in search box
  */
  searchSkill = (value) => {
    this.props.search(this.value)
  }
  /*
    Function: SearchBar.render
    @return {Component} SearchBar
  */
  render () {
    return <div className={styles.searchBar}>
      <SearchInput updateValue={this.updateValue} searchSkill={this.searchSkill} />
      <SearchButton searchSkill={this.searchSkill} />
    </div>
  }
}
