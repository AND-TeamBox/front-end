import React, {Component} from 'react'
import styles from './SearchInput.css'

/*
  Defines SearchInput as a new Component
  @prop {function} updateValue - Updates search input
  @prop {function} searchSkill - Triggers the search for certain skill
*/

export default class SearchInput extends Component {
  /*
    Function: SearchInput.valueChange
    @param {object} e - A key-up event
  */
  valueChange = (e) => {
    this.props.updateValue(this.input.value)
    if (e.keyCode === 13) {
      this.props.searchSkill()
    }
  }
  /*
    Function: SearchInput.render
    @return {Component} SearchInput
  */
  render () {
    return <input ref={(ref) => this.input = ref} type='text' onKeyUp={this.valueChange} className={styles.searchInput} placeholder="Type ANDI's skill" />
  }
}
