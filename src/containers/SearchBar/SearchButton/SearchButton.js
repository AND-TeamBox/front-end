import React, {Component} from 'react'
import styles from './SearchButton.css'

/*
  Defines SearchButoon as a new Component
  @prop {function} searchSkill - Triggers the search for certain skill
*/

export default class SearchButton extends Component {
  /*
    Function: SearchButton.search
  */
  search = () => {
    this.props.searchSkill()
  }
  /*
    Function: SearchButoon.render
    @return {Component} searchButton
  */
  render () {
    return <a onClick={this.search} className={styles.searchButton} />
  }
}
