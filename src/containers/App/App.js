import React, {Component} from 'react'
import request from 'ajax-request'
import NavBar from '../NavBar/NavBar'
import BannerHeader from '../BannerHeader/BannerHeader'
import SearchBar from '../SearchBar/SearchBar'
import Listing from '../Listing/Listing'
import Footer from '../Footer/Footer'
import Count from '../Count/Count'
import styles from './App.css'

/*
  Defines App as a new Component
*/
export default class App extends Component {
  /*
    Function: App.constructor
  */
  constructor (props) {
    super(props)
    this.state = {
      result: [],
      message: 'Enter a skill AND press search!'
    }
    this.update.bind(this)
  }

  /*
    Function: App.constructor
    @return {boolean} - stub function, currently always returns true
  */
  shouldComponentUpdate () {
    return true
  }

  /*
    Function: App.update
    @param {object} data - object containing results returned from the API
  */
  update = data => {
    const joinedData = {
      result: data.result.concat(this.state.result),
      message: data.message || ''
    }
    this.setState(joinedData)
  }

  /*
    Function: App.getUsersFromAPI
    @param {string} keyword - single keyword, e.g. css
  */
  getUsersFromAPI = (keyword = '') => {
    this.state.result = []
    const keywordsWithCommas = keyword.split(' ').join(',')
    const encodedKeyword = encodeURIComponent(keywordsWithCommas)
    request(`https://and-teambox-api.herokuapp.com/search/${encodedKeyword}`, (e, res, body) => {
      if (res.statusCode === 200) {
        const parsedBody = JSON.parse(body)
        if (!parsedBody.result.length) {
          this.sendNoresult()
        } else {
          this.update(parsedBody)
        }
      } else {
        this.sendNoresult()
      }
    })
  }

  /*
    Function: App.sendNoresult
  */
  sendNoresult = () => {
    this.update({
      result: this.state.result.length > 0 && this.state.result || [],
      message: this.state.result.length > 0 && '' || 'We tried AND didn\'t find anything!'
    })
  }

  /*
    Function: App.getSkill
    @param {object} skills - the skills list returned from the API
    @return {array} skillSet - array of objects containing main skills
  */
  getSkill = (skills) => {
    const skillSet = []
    skillSet.push({
      skill: skills.skillName,
      level: skills.skillLevel
    })
    return skillSet
  }

  /*
    Function: App.getAllSkills
    @param {object} skills - the skills list returned from the API
    @return {array} skillSet - array of objects containing all skills of a person
  */
  getAllSkills = (skillsArray) => {
    const skillSet = []
    let count = 0
    while (skillSet.length < 2 && skillsArray[count]) {
      skillSet.push({
        skill: skillsArray[count].skillName,
        level: skillsArray[count].skillLevel
      })
      count++
    }
    return skillSet
  }

  /*
    Function: App.getResults
    @return {Component} - returns the listing with name, club, and skills, or message with no results found
  */
  getresult = () => {
    if (this.state.result.length) {
      return <div>
        <Count count={this.state.result.length} />
        {this.state.result.map((item, i) => {
          return <Listing key={i} name={item.fullName} club={item.club} skills={this.getSkill(item.skillResult).concat(this.getAllSkills(item.otherSkills))}/>
        })}
      </div>
    } else {
      return <div className={styles.message}>{this.state.message}</div>
    }
  }

  /*
    Function: App.render
    @return {Component} App
  */
  render () {
    return <div className={styles.App}>
      <NavBar />
      <BannerHeader />
      <SearchBar search={this.getUsersFromAPI} />
      {this.getresult()}
      <Footer />
    </div>
  }
}
