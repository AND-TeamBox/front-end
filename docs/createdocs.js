const fs = require('fs')
const express = require('express')
const path = require('path')
const app = express()
const package = require('../package.json')
const docsPageContainer = fs.readFileSync('docs/docs-index.html', 'utf-8')

const walkSync = (dir, filelist) => {
  const files = fs.readdirSync(dir)
  filelist = filelist || []
  files.map(function(file) {
    if (fs.statSync(dir + '/' + file).isDirectory()) {
      filelist = walkSync(dir + '/' + file, filelist)
    } else if (!dir.match(/images|fonts/)) {
      filelist.push(dir + '/' + file)
    }
  })
  return filelist
}

const files = walkSync('./src')

const data = {}
files.map(function (item) {
  readFiles(item, function(filename, content) {
    data[item + '/' + filename] = content
  })
})

app.use('/', express.static(path.join(__dirname, '/')))
  .get('/', (req, res) => {
    res.send(docsPageContainer.replace('{docs}', docify(data)))
  })
  .listen(9001)

function readFiles(filename, onFileContent) {
  fs.readFile(filename, 'utf-8', function(err, content) {
    onFileContent(filename, content)
  })
}

function docify (data) {
  const content = [`
    <div class="file">
      <h1 class="app-name">Technical Docs for ${package.name}</h1>
      <div class="authors">${package.author}</div>
      <div class="packages"><ul><li>${Object.keys(package.dependencies).join('</li><li>')}</li></ul></div>
    `]
  for (let file in data) {
    const html = []
    html.push(`<h1 id='${file.split('/').pop()}'>${file.split('/').pop()}</h1>`)
    html.push(`<h3>${file}</h1>`)
    if (file.match(/\.js/)) {
      const comments = data && data[file].match(/\*(.|\n)+?\*/g)
      const imports = data && data[file].match(/import \w+ from (\-|\/|\'|\w|\.)+/g)

      if (imports) {
        for (let item in imports) {
          let importHtmlLink = imports[item].split(' ').pop().split('/').pop().replace(/\'/, '')
          if (!importHtmlLink.match(/\./)) {
            importHtmlLink = importHtmlLink + '.js'
          }
          const importHtml = `<a href="#${importHtmlLink}">` + imports[item].trim().replace('import', 'This file imports') + '</a>'
          html.push(importHtml)
        }
      }

      if (comments) {
        for (let comment in comments) {
          let htmlWithClasses = ''
          const htmlWithoutClasses = '<div>' + comments[comment].replace(/\*/g, '').trim().replace(/\s*\n\s*/g, '</div><div>') + '</div>'
          htmlWithClasses = htmlWithoutClasses
            .replace(/<div>@prop/g, '<div class="property">')
            .replace(/<div>@return/g, '<div class="returns">')
            .replace(/<div>@param/g, '<div class="param">')
            .replace(/<div>Function: /g, '<div class="function">')
            .replace(/<div>Defines/g, '<div class="defines">Defines')
          html.push(htmlWithClasses)
        }
      }
    }
    content.push(html.join('\n'))
  }
  return content.join('</div><div class="file">')
}
